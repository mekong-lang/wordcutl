# wordcutl
### _Vee Satayamas <vsatayamas@gmail.com>_

A Common Lisp wrapper for Wordcut - a Khmer, Lao, Myanmar, Thai word segmentation/breaking

## License

Apache-2.0

