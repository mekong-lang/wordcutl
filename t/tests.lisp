(defpackage #:wordcutl-tests
  (:use #:cl #:fiveam))

(in-package #:wordcutl-tests)

(def-suite basic-suite)
(in-suite basic-suite)

(test word-break-with-dict-only
  "break into words by dict only"
  (wordcutl:with-wordcut (w #P "data/thai.txt")
    (is (equal (wordcutl:wordcut-put-delimiters w "กากา" "---")
	       "กา---กา"))))

(test word-break
  "break into words by dict and cluster rules"
  (wordcutl:with-wordcut (w #P"data/thai.txt"
			  :cluster-rules-pathname #P "data/thai_cluster_rules.txt")
    (is (equal (wordcutl:wordcut-put-delimiters w "ฑัวมะ" "---")
	       "ฑัว---มะ"))))

(test word-break-into-text-ranges
  "break into ranges"
  (wordcutl:with-wordcut (w #P"data/thai.txt"
			  :cluster-rules-pathname #P "data/thai_cluster_rules.txt")
    (is (equalp (wordcutl:wordcut-into-text-ranges w "ฑัวมะ")
	       (vector (wordcutl:make-text-range :s 0 :e 3)
		       (wordcutl:make-text-range :s 3 :e 5))))))

(test word-break-into-string
  "break into strings"
  (wordcutl:with-wordcut (w #P"data/thai.txt"
			  :cluster-rules-pathname #P "data/thai_cluster_rules.txt")
    (is (equalp (wordcutl:wordcut-into-strings w "ฑัวมะ")
		(vector "ฑัว" "มะ")))))
