;;;; package.lisp

(defpackage #:wordcutl
  (:use #:cl #:cffi)
  (:export :wordcut-new :with-wordcut :wordcut-into-text-ranges
	   :wordcut-into-strings :wordcut-put-delimiters
	   :wordcut-delete :text-range :make-text-range))
