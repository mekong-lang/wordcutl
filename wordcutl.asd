;;;; wordcutl.asd

(asdf:defsystem #:wordcutl
  :description "Khmer, Lao, Myanmar, and Thai word segmentation/breaking library wrapper for Common Lisp"
  :author "Vee Satayamas <vsatayamas@gmail.com>"
  :license  "Apache-2.0"
  :version "0.0.1"
  :serial t
  :depends-on ("cffi")
  :components ((:file "package")
               (:file "wordcutl"))
  :in-order-to ((test-op (test-op "wordcutl/tests"))))

(asdf:defsystem "wordcutl/tests"
  :depends-on ("wordcutl" "fiveam" "uiop")
  :components ((:module "t"
		:components ((:file "tests"))))
  :perform (test-op (o s)
		    (uiop:symbol-call :fiveam '#:run!
				      (uiop:find-symbol* 'basic-suite 'wordcutl-tests))))
