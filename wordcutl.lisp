;;;; wordcutl.lisp

(in-package #:wordcutl)

(define-foreign-library wordcutw (t (:default "libwordcutw")))
(use-foreign-library wordcutw)

(defstruct text-range
  (s 0 :type fixnum)
  (e 0 :type fixnum))

(defcstruct c-text-range
  (s :size)
  (e :size))

(defcfun ("wordcut_new_with_dict" wordcut-new-with-dict) :pointer (path :string))
(defcfun ("wordcut_new_with_dict_and_cluster_rules" wordcut-new-with-dict-and-cluster-rules)
    :pointer (dict-path :string) (cluster-rules-path :string))
(defcfun ("wordcut_into_text_ranges" wordcut-into-text-ranges*) :pointer
  (wordcut :pointer) (text :string) (text-range-count :pointer))
(defcfun ("delete_wordcut" wordcut-delete) :void (wordcut :pointer))
(defcfun ("delete_text_ranges" delete-text-ranges) :void
  (text-ranges :pointer)
  (range-count :uint))
(defcfun ("wordcut_into_strings" wordcut-into-strings*) :pointer
  (wordcut :pointer)
  (text :string)
  (strings-count :pointer))
(defcfun ("delete_strings" delete-strings) :void (strings :pointer) (string-count :uint))
(defcfun ("wordcut_put_delimiters" wordcut-put-delimiters) :string
  (wordcut :pointer) (text :string) (delim :string))


(defun wordcut-new (dict-pathname &key cluster-rules-pathname)
  (if cluster-rules-pathname
      (wordcut-new-with-dict-and-cluster-rules (namestring dict-pathname)
					       (namestring cluster-rules-pathname))
      (wordcut-new-with-dict (namestring dict-pathname))))

(defmacro with-wordcut ((wordcut dict-pathname &key cluster-rules-pathname) &body body)
  `(let ((,wordcut (wordcut-new ,dict-pathname
				:cluster-rules-pathname ,cluster-rules-pathname)))
     (unwind-protect
	  (progn ,@body))
     (wordcut-delete ,wordcut)))

(defun wordcut-into-text-ranges (wordcut text)
  (let* ((raw-cnt (foreign-alloc :pointer))
	 (raw-ranges (wordcut-into-text-ranges* wordcut text raw-cnt))
	 (cnt (mem-ref raw-cnt :uint))
	 (text-ranges (make-array cnt :element-type 'text-range :initial-element (make-text-range))))
    (loop for i from 0 below cnt
	  do
	     (let ((ptr (mem-aref raw-ranges '(:struct c-text-range) i)))
	       (print ptr)
	       (setf (aref text-ranges i)
		     (make-text-range :s (getf ptr 'WORDCUTL::S)
				      :e (getf ptr 'WORDCUTL::E))))
	  finally
	     (foreign-free raw-cnt)
	     (delete-text-ranges raw-ranges cnt)
	     (return text-ranges))))

(defun wordcut-into-strings (wordcut text)
  (let* ((strings-count (foreign-alloc :pointer))
	 (raw-strings (wordcut-into-strings* wordcut text strings-count))
	 (cnt (mem-ref strings-count :uint))
	 (strings (make-array cnt :element-type 'string :initial-element "")))
    (loop for i from 0 below cnt
	  do (setf (aref strings i)
		   (mem-aref raw-strings :string i))
	  finally
	     (foreign-free strings-count)
	     (delete-strings raw-strings cnt)
	     (return strings))))
